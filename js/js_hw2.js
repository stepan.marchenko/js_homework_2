// When we need to execute instruction multiple times under certain condition - this is "looping".
// Loop means we can repeat the section of code as many times as possible depending the condition.
// Looping simplifies code, makes it short , clean and easy to understand ,
// because it avoids repeating of the same block of instructions in the code of a program,
// and dramatically increases productivity of the coding.

'use strict';

// Checking whether input is integer
let myInput;
do {
    myInput = +prompt('Please enter an integer multiple of five, less than 5000');
} while (myInput % 1 !== 0 || myInput >5000);

// Making array of numbers with remainder (modulus of 5) equal to 0

let myArray = [];
for (let i = 1; i <= myInput; i++) {
    if (i % 5 === 0) {
        myArray.push(i);
    }
}

// Displaying array or error message if there's no number

if (myArray.length > 0) {
    console.log(myArray);
} else {
    console.log("Sorry, no numbers");
}